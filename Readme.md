# lowkey-video-converter
Convert lowkey json files containing bookmarks to files with correct ino.

## How to use
Run 'yarn start' to check the command syntax.

### Note
All file names refer to files in the 'build' directory.