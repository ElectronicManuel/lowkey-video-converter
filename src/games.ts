import { LowkeyVideo } from "./types";

export const GAME_DATA: {
    [gameId: string]: LowkeyVideo['game']
} = {
    ['13812321-a1b5-4212-be12-32ac268f5d46']: {
        "uuid": "13812321-a1b5-4212-be12-32ac268f5d46",
        "name": "League of Legends",
        "priority": 1,
        "imageUrl": "http://d1hw8mrohsmlt5.cloudfront.net/games/lol.png",
        "executable": "league of legends.exe"
    }
}