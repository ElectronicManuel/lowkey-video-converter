import fs from 'fs/promises';
import path from 'path';
import { LowkeySaveFile, LowkeyVideo } from '../types';

export const moveDirCommand = async (base: string, targetDir: string, output: string) => {
    console.log('Reading lowkey files...');
    const baseFileRaw = await fs.readFile(base, 'utf-8');
    console.log('Files loaded. Parsing...')

    const baseFileJson: LowkeySaveFile = JSON.parse(baseFileRaw);
    console.log(`Files parsed. ${baseFileJson.videos.length} videos in base.`)

    const targetFiles = await fs.readdir(targetDir);
    console.log(`Directory read, ${targetFiles.length} files.`)

    const targetFileInfos: Array<{ path: string, name: string, ino: number }> = []

    for(const targetFile of targetFiles) {
        const filePath = path.resolve(targetDir, targetFile);
        const fileStats = await fs.stat(filePath);
        targetFileInfos.push({
            name: targetFile,
            path: filePath,
            ino: fileStats.ino
        })
    }

    let matchedCount = 0;
    let unmatchedCount = 0;

    const editedVideos: LowkeyVideo[] = [];
    for(const video of baseFileJson.videos) {
        const matchingNewFile = targetFileInfos.find(v => v.name === video.title);
        if(!matchingNewFile) {
            editedVideos.push(video);
            unmatchedCount++;
            continue;
        }

        matchedCount++;

        editedVideos.push({
            ...video,
            filePath: matchingNewFile.path,
            ino: matchingNewFile.ino,
            videoUrl: `file://${matchingNewFile.path}`
        })
    }

    console.log(`Done. ${matchedCount} base videos updated for new directory. ${unmatchedCount} base videos untouched.`)
    console.log(`Writing output file...`)

    const updatedObject = {
        ...baseFileJson,
        videos: editedVideos.sort((a, b) => a.createdAt.localeCompare(b.createdAt))
    }

    await fs.writeFile(output, JSON.stringify(updatedObject, null, '\t'), 'utf-8')

    console.log(`Output file written: ${output}`)
};