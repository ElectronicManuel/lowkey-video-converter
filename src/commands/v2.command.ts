import fs from 'fs/promises';
import { GAME_DATA } from '../games';
import { LowkeySaveFile, LowkeyV2SaveFile, LowkeyVideo } from '../types';

export const v2Command = async (base: string, input: string, output: string) => {
    console.log('Reading lowkey files...');
    const baseFileRaw = await fs.readFile(base, 'utf-8');
    const inputFileRaw = await fs.readFile(input, 'utf-8');
    console.log('Files loaded. Parsing...')

    const baseFileJson: LowkeySaveFile = JSON.parse(baseFileRaw);
    const inputFileJson: LowkeyV2SaveFile = JSON.parse(inputFileRaw);
    console.log(`Files parsed. ${baseFileJson.videos.length} videos in base. ${inputFileJson.objects.length} v2 videos in input.`)

    let matchedCount = 0;
    let unmatchedCount = 0;
    let noMatchDataCount = 0;

    const editedVideos: LowkeyVideo[] = [];
    for(const video of baseFileJson.videos) {
        const alreadyHasMatchData = !!video.matchData;
        const matchingV2InputVideo = inputFileJson.objects.find(v => v.title === video.title);
        if(!matchingV2InputVideo) {
            editedVideos.push(video);
            unmatchedCount++;
            if(!alreadyHasMatchData) noMatchDataCount++;
            continue;
        }

        if(!matchingV2InputVideo.match) {
            editedVideos.push(video);
            unmatchedCount++;
            if(!alreadyHasMatchData) noMatchDataCount++;
            continue;
        }
        matchedCount++;

        // check for matching game
        const matchingGame = GAME_DATA[matchingV2InputVideo.gameId] || GAME_DATA['13812321-a1b5-4212-be12-32ac268f5d46'] // check for game or use league as a fallback

        // shorthand for v2 match data
        const v2MatchData = matchingV2InputVideo.match;

        // this formula is used to calculate the offset the timestamps are misaligned by
        const offset = (video.lengthInMilliseconds - v2MatchData.gameDuration*1000) - (matchingV2InputVideo.recordingStartedAt - v2MatchData.gameCreation)

        // put video in array
        editedVideos.push({
            ...video,
            checkedForMatchData: true,
            matchData: {
                championName: v2MatchData.championName,
                kda: v2MatchData.kda,
                championImg: v2MatchData.championImg,
                gameCreation: v2MatchData.gameCreation,
                gameDuration: v2MatchData.gameDuration,
                win: v2MatchData.win,
                matchEvents: !v2MatchData.notableEvents ? [] : v2MatchData.notableEvents.map(event => {
                    let mappedTimestamp = Math.floor((event.timestamp+offset) / 1000) // in order to properly map the v2 timestamp to a v3 timestamp, we need to apply the offset and then divide by 1000
                    return {
                        timestamp: mappedTimestamp,
                        eventType: event.eventType as any
                    }
                })
            },
            game: matchingGame,
            recordingStartedAt: matchingV2InputVideo.recordingStartedAt
        })
    }

    console.log(`Done. ${matchedCount} base videos updated with v2 input. ${unmatchedCount} base videos untouched.`)
    console.log(`Output file has ${noMatchDataCount} videos without matchdata.`)
    console.log(`Writing output file...`)


    const updatedObject = {
        ...baseFileJson,
        videos: editedVideos
    }

    await fs.writeFile(output, JSON.stringify(updatedObject, null, '\t'), 'utf-8')

    console.log(`Output file written: ${output}`)
}