import fs from 'fs/promises';
import { LowkeySaveFile, LowkeyVideo } from '../types';

export const patchCommand = async (base: string, input: string, output: string) => {
    console.log('Reading lowkey files...');
    const baseFileRaw = await fs.readFile(base, 'utf-8');
    const inputFileRaw = await fs.readFile(input, 'utf-8');
    console.log('Files loaded. Parsing...')

    const baseFileJson: LowkeySaveFile = JSON.parse(baseFileRaw);
    const inputFileJson: LowkeySaveFile = JSON.parse(inputFileRaw);
    console.log(`Files parsed. ${baseFileJson.videos.length} videos in base. ${inputFileJson.videos.length} videos in input.`)

    let matchedCount = 0;
    let unmatchedCount = 0;
    let noMatchDataCount = 0;

    const editedVideos: LowkeyVideo[] = [];
    for(const video of baseFileJson.videos) {
        const alreadyHasMatchData = !!video.matchData;
        const matchingInputVideo = inputFileJson.videos.find(v => v.title === video.title);
        if(!matchingInputVideo) {
            editedVideos.push(video);
            unmatchedCount++;
            if(!alreadyHasMatchData) noMatchDataCount++;
            continue;
        }

        if(!matchingInputVideo.matchData) {
            editedVideos.push(video);
            unmatchedCount++;
            if(!alreadyHasMatchData) noMatchDataCount++;
            continue;
        }
        matchedCount++;

        editedVideos.push({
            ...video,
            checkedForMatchData: true,
            matchData: matchingInputVideo.matchData,
            game: matchingInputVideo.game,
            createdAt: matchingInputVideo.createdAt,
            recordingStartedAt: matchingInputVideo.recordingStartedAt
        })
    }

    console.log(`Done. ${matchedCount} base videos updated with input. ${unmatchedCount} base videos untouched.`)
    console.log(`Output file has ${noMatchDataCount} videos without matchdata.`)
    console.log(`Writing output file...`)

    const updatedObject = {
        ...baseFileJson,
        videos: editedVideos.sort((a, b) => a.createdAt.localeCompare(b.createdAt))
    }

    await fs.writeFile(output, JSON.stringify(updatedObject, null, '\t'), 'utf-8')

    console.log(`Output file written: ${output}`)
};