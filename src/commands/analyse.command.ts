import fs from 'fs/promises';
import { LowkeySaveFile, LowkeyVideo } from '../types';

export const analyseCommand = async (base: string) => {
    console.log('Reading lowkey files...');
    const baseFileRaw = await fs.readFile(base, 'utf-8');
    console.log('Files loaded. Parsing...')

    const baseFileJson: LowkeySaveFile = JSON.parse(baseFileRaw);
    console.log(`Files parsed. ${baseFileJson.videos.length} videos in config.`)

    let v2Count = 0;
    let v3Count = 0;

    let matchDataCount = 0;
    let noMatchDataCount = 0;

    let matchDataCountV2 = 0;
    let noMatchDataCountV2 = 0;

    let matchDataCountV3 = 0;
    let noMatchDataCountV3 = 0;

    const noMatchDataList: LowkeyVideo[] = [];

    for(const video of baseFileJson.videos) {
        const alreadyHasMatchData = !!video.matchData;

        const isV2 = video.title.includes('-');
        // v2 name League_of_Legends_06-15_00015121.mkv
        // v3 name League_of_Legends_1627849164917.mkv
        if(isV2) {
            v2Count++;
        } else {
            v3Count++;
        }

        if(alreadyHasMatchData) {
            matchDataCount++;
            if(isV2) {
                matchDataCountV2++;
            } else {
                matchDataCountV3++;
            }
        } else {
            noMatchDataCount++;
            if(isV2) {
                noMatchDataCountV2++;
            } else {
                noMatchDataCountV3++;
            }
            noMatchDataList.push(video);
        }

        
    }

    const videoPercentage = Math.floor((100 / baseFileJson.videos.length) * matchDataCount);
    const videoPercentageV2 = Math.floor((100 / v2Count) * matchDataCountV2);
    const videoPercentageV3 = Math.floor((100 / v3Count) * matchDataCountV3);

    console.log(`v2 videos: ${v2Count}. v3 videos: ${v3Count}.`)
    
    console.log(`total v2: ${v2Count}. match data percentage: ${videoPercentageV2}% (${matchDataCountV2} w, ${noMatchDataCountV2} w/o).`)
    console.log(`total v3: ${v3Count}. match data percentage: ${videoPercentageV3}% (${matchDataCountV3} w, ${noMatchDataCountV3} w/o).`)

    console.log(`total videos: ${baseFileJson.videos.length}. match data percentage: ${videoPercentage}% (${matchDataCount} w, ${noMatchDataCount} w/o).`)
    console.log(`v3 videos without match data: `, JSON.stringify(noMatchDataList.filter(v => !v.title.includes('-')).map(v => `${v.title} ${v.createdAt}`), null, '\t'));
};