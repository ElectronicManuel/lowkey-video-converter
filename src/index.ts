import yargs from 'yargs/yargs';
import { analyseCommand } from './commands/analyse.command';
import { moveDirCommand } from './commands/movedir.command';
import { patchCommand } from './commands/patch.command';
import { v2Command } from './commands/v2.command';
import { buildFile } from './util';

yargs(process.argv.slice(2))
    .scriptName('lowkey-convert')
    .option('output', {
        describe: 'output file being written',
        default: 'lowkey_output.json',
        type: 'string'
    })
    .command('merge', 'merge two lowkey configs.', {
        base: {
            describe: 'the base config containing all the stored videos',
            default: 'lowkey_base.json',
            type: 'string'
        },
        input: {
            describe: 'the input file containing the bookmarks to be added',
            default: 'lowkey_input.json',
            type: 'string'
        }
    }, async argv => {
        console.log('Patching lowkey config...')
        await patchCommand(buildFile(argv.base), buildFile(argv.input), buildFile((argv as any).output));
    })
    .command('v2', 'merge a v2 lowkey config into a v3 one', {
        base: {
            describe: 'the source config containing all the stored videos',
            default: 'lowkey_base.json',
            type: 'string'
        },
        input: {
            describe: 'the v2 config file',
            default: 'lowkey_v2.json',
            type: 'string'
        }
    }, async argv => {
        console.log('Merging v2 config into v3...')
        await v2Command(buildFile(argv.base), buildFile(argv.input), buildFile((argv as any).output));
    })
    .command('analyse', 'analyses a lowkey config file.', {
        base: {
            describe: 'the source config containing all the stored videos',
            default: 'lowkey_base.json',
            type: 'string'
        },
    }, async argv => {
        console.log('analysing config file...')
        await analyseCommand(buildFile(argv.base));
    })
    .command('movedir', 'scan target directory and update base config with inos of new directory.', {
        base: {
            describe: 'the source config containing all the stored videos',
            default: 'lowkey_base.json',
            type: 'string'
        },
        target: {
            describe: 'the target directory to scan',
            demandOption: true,
            type: 'string'
        },
    }, async argv => {
        console.log('beginning directory move...')
        await moveDirCommand(buildFile(argv.base), argv.target, buildFile((argv as any).output));
    })
    .demandCommand()
    .argv