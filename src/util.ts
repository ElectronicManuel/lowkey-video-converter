import path from 'path';

export const buildFile = (fileName: string) => path.join(__dirname, '..', 'build', fileName);