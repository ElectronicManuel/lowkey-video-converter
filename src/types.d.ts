export interface LowkeyMatchEvent {
    timestamp: number
    eventType: 'kill' | 'death' | 'assist' | 'riftherald' | 'dragon' | 'baron'
}

export interface LowkeyMatchData {
    championName: string
    kda: string
    championImg: string
    gameCreation: number
    gameDuration: number
    win: boolean
    matchEvents: Array<LowkeyMatchEvent>
}

export interface LowkeyVideo {
    ino: number
    filePath: string
    thumbnailUrls: string[]
    title: string
    videoUrl: string
    lengthInMilliseconds: number
    recordingStartedAt: number
    createdAt: string
    game: {
        uuid: string
        name: string
        priority: number
        imageUrl: string
        executable: string
    }
    checkedForMatchData: boolean
    matchData: LowkeyMatchData
}

export interface LowkeySaveFile {
    videos: LowkeyVideo[]
}

// V2

export interface LowkeyV2Video {
    title: string,
    bookmarks: string[]
    duration: number
    recordingStartedAt: number
    lolLocalSettings: {
      lolRegion: string
    },
    lolUsername: string,
    gameId: string,
    match: {
      championId: number,
      lolUsername: string,
      championName: string,
      championImg: string,
      gameCreation: number,
      gameDuration: number,
      win: boolean,
      kda: string,
      notableEvents: Array<{
        type: string,
        timestamp: number,
        position: {
          x: number,
          y: number
        },
        killerId: number,
        victimId: number,
        assistingParticipantIds: number[],
        eventType: string
      }>
    }
}

export interface LowkeyV2SaveFile {
    objects: Array<LowkeyV2Video>
}